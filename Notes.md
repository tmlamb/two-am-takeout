# Requirements

> There is a test for the backend that is failing, fix the app code so it passes (hint: it's related to struct tags)

- TestHealthCheck is failing because HealthBody handler returns 'OK' in all caps, and the test asserts 'ok'. Changing the HealthBody struct to use 'ok' in the serialized json fixed the failing test.

> There is a test for the backend where the assumptions about what needs to be tested are incorrect. The test passes, but doesn't guarantee compatibility with how the frontend expects to communicate. You need to think about how the two interact and are compatible, and change the test code to match.

- First step taken to is review the client and server code, and get them running locally using the README. Also setup Vue extensions in VS Code (first timer here!)
- Confirmed healthchecks between client and server are working, and messages can be sent.
- Setup launch file in vscode to debug the go server. Used the debugger to confirm the format of messages be sent from the client:

```
"{\"text\":\"test\",\"uuid\":\"3c4d8a63-2391-4c4a-9746-bcfd4cd23dff\"}"
```

From the way TestWebsockets is written, I think it is a problem with how the test expects the message to be a simple string with the message text, while the client is sending a JSON object containing both text and a uuid. Debugging the same line of code while running the test confirms that the test sends the message in a different format:

```
"Hello, Websocket!"
```

- For a change to improve the test, I think it may be enough to check that the server can receive JSON without mangling the message or throwing an error, so I modified the test to send a message in JSON format with a structure similar to client.

- With more time, I think this code could be improved to more strictly enforce a structure to this JSON object.

> In the frontend, make it so the user can enter a name that gets sent with every message and displayed to other users.

- Assumption: User will be given an additional text field above the New Message text field where they can enter their name, which will then be included with every message. Ideally in a final version the username would be part of the auth context, but as a simple POC we will let the user decide their own name on the fly.
- First step is to add a text input name and update the "sendMessage" function to include the username in the json sent to the server. Debugging on the server confirms that the username is present in the JSON.
- At this point it seems necessary to check the behavior of the socket communications between two clients, so I brought up the client in a different browser, and can see messages from the two connections appearing correctly, with the new username field appearing in the JSON (but still not displayed)
- Adding the username field to the content displayed next to each message sent by other users brings us the rest of the way there! I'll also assume the user doesn't care to see their own name next to their own messages.

> There is a security issue in the socket server. Identify and fix it. (Hint, it's not a problem in the health check).

- I'm not much of a golang or websockets expert, but I think I've seen this exact "solution" in other server code:

	```go
  // Cross-Origin stuff is a pain
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
  ```
The gorilla docs describe how this should work:
> The Upgrader calls the function specified in the CheckOrigin field to check the origin. If the CheckOrigin function returns false, then the Upgrade method fails the WebSocket handshake with HTTP status 403.

> If the CheckOrigin field is nil, then the Upgrader uses a safe default: fail the handshake if the Origin request header is present and the Origin host is not equal to the Host request header.

This is the type of thing a developer might add to quickly resolve problems they are having without understanding the underlying security issues. A quick for to avoid issues down the road, and allow local testing to continue, would be to have a simple check to see if it's our local client connecting (see my commit)

A robust solution could be lift the allowed origin state as global config in the app, so it can be shared between this CheckOrigin function and the CORS middleware configured in handlers.go, so that later when the app is productionalized it would be a quick process to add the testing/prod frontend domains.

> Put your own style on the frontend.

Time check and I'm about at two hours, but I spent a few minutes looking at the Vuetify docs to learn what a v-chip is so I could clean up the UI and make it more clear that the username is separate from the message.

# Other changes

- Before tests would run I needed to run `go mod tidy` to add missing indirect dependencies.
